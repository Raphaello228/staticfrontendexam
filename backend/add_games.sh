#!/bin/bash

# Add game data to the backend

# To clear the existing data please use the following command:
#   curl -X DELETE {url}

FILE='chess.pgn'

if [ $# = 0 ]
then
    echo "Please supply a url for posting the game data."
else

    # Read the contents of the PGN file
    pgn=$(cat "$FILE")

    # Separate each game in the PGN file
    readarray -t games < <(echo "$pgn" | awk 'BEGIN { RS = "" } //')

    # Initialize index variables
    start=0
    end=10
    json_array="["

    for (( i=0; i<29; i++ ))
        do
          # Create a new JSON object
          json='{ "type": "Game", "fields": {'
          movements=""
          # Add attributes to JSON object
          for (( j=start; j<=end; j++ ))

          do
            if (( j == start )); then
              attribute='"Event": "'$(echo "${games[$j]#*[Ee][Vv][Ee][Nn][Tt]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+1 )); then
              attribute='"Site":"'$(echo "${games[$j]#*[Ss][Ii][Tt][Ee]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+2 )); then
              attribute='"Date":"'$(echo "${games[$j]#*[Dd][Aa][Tt][Ee]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+3 )); then
              attribute='"Round":"'$(echo "${games[$j]#*[Rr][Oo][Uu][Nn][Dd]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+4 )); then
              attribute='"White":"'$(echo "${games[$j]#*[Ww][Hh][Ii][Tt][Ee]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+5 )); then
              attribute='"Black":"'$(echo "${games[$j]#*[Bb][Ll][Aa][Cc][Kk]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+6 )); then
              attribute='"Result":"'$(echo "${games[$j]#*[Rr][Ee][Ss][Uu][Ll][Tt]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+7 )); then
              attribute='"WhiteElo":"'$(echo "${games[$j]#*[Ww][Hh][Ii][Tt][Ee][Ee][Ll][Oo]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+8 )); then
              attribute='"BlackElo":"'$(echo "${games[$j]#*[Bb][Ll][Aa][Cc][Kk][Ee][Ll][Oo]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            elif (( j == start+9 )); then
              attribute='"ECO":"'$(echo "${games[$j]#*[Ee][Cc][Oo]}" | sed 's/"//g;s/\[//g;s/\]//g')'"'
            else
              movements='"moves":"'${games[$j]}'"'
            fi

            if((j != start + 10)); then
            json="$json $attribute,"
            fi



          done
          json=${json%,} # Remove trailing comma
          json_array+="$json }, $movements },"
          json_to_send="$json }, $movements }"
          echo $json_to_send
          curl -X POST -H "Content-Type: application/json" -d "$json_to_send" http://0.0.0.0:5000/games
          # Update index variables
                start=$(( end+1 ))
                end=$(( end+11 ))
          done


    json_array=${json_array%,} # Remove trailing comma
    json_array+=" ]"

    echo $json_array  > bob.json

    #echo "$1"



    # TODO: Write a script to send a list of games to the backend.
    #       Use curl to send the game data to the server application.
    #       Don't forget to set the content-type to `application/json` in your curl command.
    #
    # The game data should be read from the provided 'chess.pgn' file.

fi
